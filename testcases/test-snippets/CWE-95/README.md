# CWE-95: Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')

## Description Summary
The software receives input from an upstream component, but it does not neutralize or incorrectly neutralizes code syntax before using the input in a dynamic evaluation call (e.g. "eval").
## Extended Description
This may allow an attacker to execute arbitrary code, or at least modify what code can be executed.