# Test cases

These test cases are downloaded from samate.nist.gov and they are part of the Test Suite #103: PHP Vulnerability Test Suite.
Tests can be found here https://samate.nist.gov/SARD/view.php?tsID=103. I reorganized test cases into directories by their proper CWE id and added description taken from cwe.mitre.org for every CWE.
Currently there are 42212 test cases and they are covering most common security weakness categories, including XSS, SQL injection, URL redirection, etc.
The directory structure follow this pattern:
 
* CWE-Number/
  * Safe/
    * Test cases
  * Unsafe/
    * Test cases
   
As can be seen, there are Safe and Unsafe test cases, Safe examples are also included because they can be tested against static code analysers in order to find false positives and improve further detection.
