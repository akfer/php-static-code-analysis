# CWE-311: Missing Encryption of Sensitive Data

# Description Summary
The software does not encrypt sensitive or critical information before storage or transmission.
## Extended Description
The lack of proper data encryption passes up the guarantees of confidentiality, integrity, and accountability that properly implemented encryption conveys.