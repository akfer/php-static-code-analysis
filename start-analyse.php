<?php
$currentDir = getcwd();
$dirsToScan = getScanDirs();

foreach($dirsToScan as $dirToScan){
	$reportDir = createReportDir($dirToScan);
	echo "Executing static code analysers on directory " . $dirToScan . "...\n";

	echo "Executing RIPS...\n";
	exec('cp -R static-code-analyzers/rips-0.55-cli/css/ '. $reportDir.'/css'); //Copy files for RIPS report
	exec('cp -R static-code-analyzers/rips-0.55-cli/js/ '. $reportDir.'/js'); //Copy files for RIPS report
	exec("php static-code-analyzers/rips-0.55-cli/index.php loc=".$dirToScan." subdirs=1 vector=all verbosity=1 > ".$reportDir. '/rips-scan.html');
	
	echo "Executing OWASP WAP...\n";
	chdir($currentDir . '/static-code-analyzers/wap-2.1/');	//for some reason wap don't work properly if the directory is not changed
	$descriptorspec = array(
	   0 => array("pipe", "r"),	// stdin is a pipe that the child will read from
	   1 => array("pipe", "w"),	// stdout is a pipe that the child will write to
	   2 => array("pipe", "w")	// stderr is a pipe that the child will write to
	);
	flush();
	$process = proc_open('./wap -a -all -s -out '. $reportDir .'/wap-scan.txt -p '. $dirToScan, $descriptorspec, $pipes, realpath('./'), array());
	if (is_resource($process)) {
	    while ($s = fgets($pipes[1])) {
		if(strpos($s, "enter")){
			fwrite($pipes[0], "\r");	//OWASP WAP asks user to press enter in order to finish	
		}
		flush();
	    }
	}

	echo "Executing Parse...\n";
	chdir($currentDir . '/static-code-analyzers/parse/bin/');
	exec("./psecio-parse scan ". $dirToScan. " > ". $reportDir."/parse-scan.txt");
	chdir($currentDir);
}

function getScanDirs(){
	$dirsToScan = [];
	$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('./testcases/'), RecursiveIteratorIterator::SELF_FIRST);

	foreach($iterator as $file) {
		if($file->isDir()) {
			$directory = $file->getRealpath();
			if(file_exists($directory.'/.stop') && !in_array($directory, $dirsToScan)){
				$dirsToScan[] = $directory;	
			}
		}
	}

	return $dirsToScan;
}

function createReportDir($dirToScan){
	$reportDir = str_replace("testcases", "testcases-reports", $dirToScan);
	if(!file_exists($reportDir)){
		mkdir($reportDir, 0755, true);
	}
	return $reportDir;
}

?>
