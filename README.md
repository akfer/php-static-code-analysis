# Description

This is a repository for my Bachelor's Thesis project. The title of my project is Testing Tools for Static Code Analysis of Applications Written in PHP Programming Language.

The test cases can be found in the testcases folder, while the static code analyzers can be found in the static-code-analyzers folder.